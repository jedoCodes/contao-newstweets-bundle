<?php

/*
 * This file is part of NewsTweetsBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     Contao4 NewsTweetsBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */

$translator = \System::getContainer()->get('translator');

$GLOBALS['TL_DCA']['tl_news_twitter_accounts'] = [
    'config' => [
        'label' => [
            $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.new.0'),
            $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.new.1'),
        ],
        'dataContainer' => 'Table',
        'enableVersioning' => true,
        'doNotCopyRecords' => true,
        'backlink' => 'do=news',
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],
    'list' => [
        'sorting' => [
            'mode' => 1,
            'fields' => ['account'],
            'flag' => 1,
            'panelLayout' => 'filter;search',
        ],
        'label' => [
            'fields' => ['account', 'user_id', 'addUrl'],
            'format' => '%s %s %s',
            'showColumns' => true,
            'label_callback' => ['jedoslabs.newstweets.listener.data_container.news_twitter_accounts_listener', 'onLabelCallback'],
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => [
                    $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.edit.0'),
                    $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.edit.1'),
                ],
                'href' => 'act=edit',
                'icon' => 'edit.svg',
            ],
            'delete' => [
                'label' => [
                    $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.delete.0'),
                    $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.delete.1'),
                ],
                'href' => 'act=delete',
                'icon' => 'delete.svg',
                'attributes' => 'onclick="if(!confirm(\''.$GLOBALS['TL_LANG']['MSC']['deleteConfirm'].'\'))return false;Backend.getScrollOffset()"',
            ],
            'toggle' => [
                'label' => [
                    $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.toggle.0'),
                    $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.toggle.1'),
                ],
                'icon' => 'visible.svg',
                'attributes' => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback' => ['jedoslabs.newstweets.listener.data_container.news_twitter_accounts_listener', 'toggleIcon'],
            ],
            'show' => [
                'label' => [
                    $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.show.0'),
                    $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.show.1'),
                ],
                'href' => 'act=show',
                'icon' => 'show.svg',
            ],
        ],
    ],
    'palettes' => [
        '__selector__' => ['addUrl', 'activateBitly'],
        'default' => '{account_legend},account,user_id;{settings_legend},consumerKey,consumerSecret,accessKey,accessSecret;{tweetsettings_legend},addUrl;{publish_legend},published',
    ],
    'subpalettes' => [
        'addUrl' => 'urlPrefix,useShortUrl,activateBitly',
        'activateBitly' => 'bitlyLogin,bitlyAPIKey',
    ],
    'fields' => [
        'id' => [
            'sql' => ['type' => 'integer', 'unsigned' => true, 'autoincrement' => true],
        ],
        'tstamp' => [
            'sql' => ['type' => 'integer', 'unsigned' => true, 'default' => 0],
        ],
        'account' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.account.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.account.1'),
            ],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => ['type' => 'string', 'default' => ''],
        ],
        'user_id' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.user_id.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.user_id.1'),
            ],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => false, 'rgxp' => 'natural', 'maxlength' => 255, 'tl_class' => 'w50'],
            'load_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'decryptData'],
            ],
            'save_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'encryptData'],
            ],
            'sql' => ['type' => 'string', 'default' => ''],
        ],
        'consumerKey' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.consumerKey.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.consumerKey.1'),
            ],
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'rgxp' => 'alnum', 'tl_class' => 'w50'],
            'load_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'decryptData'],
            ],
            'save_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'encryptData'],
            ],
            'sql' => ['type' => 'string', 'default' => ''],
        ],
        'consumerSecret' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.consumerSecret.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.consumerSecret.1'),
            ],
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'rgxp' => 'alnum', 'tl_class' => 'w50'],
            'load_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'decryptData'],
            ],
            'save_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'encryptData'],
            ],
            'sql' => ['type' => 'string', 'default' => ''],
        ],
        'accessKey' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.accessKey.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.accessKey.1'),
            ],
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'rgxp' => 'alnum', 'tl_class' => 'w50'],
            'load_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'decryptData'],
            ],
            'save_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'encryptData'],
            ],
            'sql' => ['type' => 'string', 'default' => ''],
        ],
        'accessSecret' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.accessSecret.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.accessSecret.1'),
            ],
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'rgxp' => 'alnum', 'tl_class' => 'w50'],
            'load_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'decryptData'],
            ],
            'save_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'encryptData'],
            ],
            'sql' => ['type' => 'string', 'default' => ''],
        ],
        'addUrl' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.addUrl.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.addUrl.1'),
            ],
            'inputType' => 'checkbox',
            'eval' => ['mandatory' => false, 'submitOnChange' => true, 'tl_class' => 'w50 m12 clr'],
            'sql' => ['type' => 'boolean', 'default' => 0],
        ],
        'urlPrefix' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.urlPrefix.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.urlPrefix.1'),
            ],
            'inputType' => 'text',
            'eval' => ['mandatory' => false, 'rgxp' => 'url', 'tl_class' => 'w50'],
            'sql' => ['type' => 'string', 'default' => ''],
        ],
        'useShortUrl' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.useShortUrl.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.useShortUrl.1'),
            ],
            'inputType' => 'checkbox',
            'eval' => ['mandatory' => false, 'tl_class' => 'w50 m12'],
            'sql' => ['type' => 'boolean', 'default' => 0],
        ],
        'activateBitly' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.activateBitly.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.activateBitly.1'),
            ],
            'inputType' => 'checkbox',
            'eval' => ['mandatory' => false, 'submitOnChange' => true, 'tl_class' => 'w50 m12 clr'],
            'sql' => ['type' => 'boolean', 'default' => 0],
        ],
        'bitlyLogin' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.bitlyLogin.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.bitlyLogin.1'),
            ],
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'rgxp' => 'alnum', 'tl_class' => 'w50'],
            'load_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'decryptData'],
            ],
            'save_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'encryptData'],
            ],
            'sql' => ['type' => 'string', 'default' => ''],
        ],
        'bitlyAPIKey' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.bitlyAPIKey.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.bitlyAPIKey.1'),
            ],
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'rgxp' => 'alnum', 'tl_class' => 'w50'],
            'load_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'decryptData'],
            ],
            'save_callback' => [
                ['JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil', 'encryptData'],
            ],
            'sql' => ['type' => 'string', 'default' => ''],
        ],
        'published' => [
            'label' => [
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.published.0'),
                $translator->trans('jedolabs.newstweets.tl_news_twitter_accounts.published.1'),
            ],
            'exclude' => true,
            'filter' => true,
            'inputType' => 'checkbox',
            'sql' => "char(1) NOT NULL default ''",
        ],
    ],
];
