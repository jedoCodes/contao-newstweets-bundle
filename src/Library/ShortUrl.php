<?php

/*
 * This file is part of NewsTweetsBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     Contao4 NewsTweetsBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */

declare(strict_types=1);

namespace JedoLabs\ContaoNewstweetsBundle\Library;

class ShortUrl
{
    public function getUrl($objItem, $urlPrefix)
    {
        switch ($objItem['source']) {
            // Link to external page
            case 'external':

                if ('mailto:' === substr($objItem['url'], 0, 7)) {
                    $url = \StringUtil::encodeEmail($objItem['url']);
                } else {
                    $url = ampersand($objItem['url']);
                }
                break;

            // Link to an internal page
            case 'internal':
                $objPage = \Database::getInstance()->prepare('SELECT id, alias FROM tl_page WHERE id=?')->limit(1)->execute($objItem['jumpTo']);
                if ($objPage->numRows) {
                    $url = ampersand(\Frontend::generateFrontendUrl($objPage->row()));
                }
                break;

            // Link to an article
            case 'article':
                $objPage = \Database::getInstance()->prepare('SELECT a.id AS aId, a.alias AS aAlias, a.title, p.id, p.alias FROM tl_article a, tl_page p WHERE a.pid=p.id AND a.id=?')->limit(1)->execute($objItem['articleId']);
                if ($objPage->numRows) {
                    $url = ampersand(\Frontend::generateFrontendUrl($objPage->row(), '/articles/'.((!$GLOBALS['TL_CONFIG']['disableAlias'] && \strlen($objPage->aAlias)) ? $objPage->aAlias : $objPage->aId)));
                }
                break;

            default: //Link zum News-Artikel
                //Seite laden
                $objPage = \Database::getInstance()->prepare('SELECT id, alias FROM tl_page WHERE id=?')->limit(1)->execute($objItem['jumpTo']);

                if ($objPage->numRows) {
                    $url = ampersand(\Frontend::generateFrontendUrl($objPage->row(), '/items/'.((!$GLOBALS['TL_CONFIG']['disableAlias'] && \strlen($objItem['alias'])) ? $objItem['alias'] : $objItem['id'])));
                } else {
                    $url = ampersand(\Environment::get('request'), true);
                }
        }

        return self::getUrlPrefix($urlPrefix).$url;
    }

    public function getTinyUrl($url)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url='.$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function getBitlyUrl($url, $login, $apiKey)
    {
        $requestUrl = 'http://api.bitly.com/v3/shorten?login='.$login.'&apiKey='.$apiKey.'&format=json&uri='.$url;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $requestUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        // Rückgabe analog zu Listing 4 verarbeiten
        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        return $response->data->url;
    }

    private function getUrlPrefix($urlPrefix)
    {
        //URL-Prex anhen
        if ('' !== trim($urlPrefix)) {
            //nehme hinterlegten Präfix für das Archiv
            if ('/' !== substr($urlPrefix, -1)) {
                $url = $urlPrefix.'/';
            } else {
                $url = $urlPrefix;
            }
        } else {
            //nehme Base-Url
            $url = \Environment::get('base');
        }

        return $url;
    }
}
