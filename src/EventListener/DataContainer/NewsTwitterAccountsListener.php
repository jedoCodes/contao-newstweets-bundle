<?php

/*
 * This file is part of NewsTweetsBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     Contao4 NewsTweetsBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */


namespace JedoLabs\ContaoNewstweetsBundle\EventListener\DataContainer;

//use JedoLabs\ContaoExtensionHelperBundle\Security\EncryptionUtil;
use Contao\Controller;
use Contao\System;

use JedoLabs\ContaoNewstweetsBundle\Model\NewsTwitterAccountsModel;

class NewsTwitterAccountsListener
{

    public function onLabelCallback($row, $label)
    {
        $image = 'editor';
        if (!$row['published']) {
            $image .= '_';
        }
        if (!$row['addUrl']) {
            $addUrl = \System::getContainer()->get('translator')->trans('jedolabs.newstweets.tl_news_twitter_accounts.addUrlOption.0');
        } else {
            $addUrl = \System::getContainer()->get('translator')->trans('jedolabs.newstweets.tl_news_twitter_accounts.addUrlOption.1');
        }

        $args[0] = $row['account'];
        //$args[1] = EncryptionUtil::decryptData($row['user_id']);
        $args[1] = \System::getContainer()->get('jedolabs.extensionhelper.security.encryption')->decryptData($row['user_id']);
        $args[2] = $addUrl;

        return $args;
    }

    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {

        if (strlen(\Contao\Input::get('tid'))) {
            $this->toggleVisibility(\Contao\Input::get('tid'), ('1' === \Contao\Input::get('state')), (@func_get_arg(12) ?: null));
            Controller::redirect(System::getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!\Contao\BackendUser::getInstance()->hasAccess('NewsTwitterAccountsModel::getTable()::published', 'alexf')) {
            return '';
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);

        if (!$row['published']) {
            $icon = 'invisible.svg';
        }

        return '<a href="'.Controller::addToUrl($href).'&rt='.\RequestToken::get().'" title="'.\StringUtil::specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label, 'data-state="'.($row['published'] ? 1 : 0).'"').'</a> ';
    }




    public function toggleVisibility($intId, $blnVisible, \DataContainer $dc = null)
    {
        \Input::setGet('id', $intId);
        \Input::setGet('act', 'toggle');

        if ($dc) {
            $dc->id = $intId;
        }

        if (!\BackendUser::getInstance()->hasAccess(NewsTwitterAccountsModel::getTable().'::published', 'alexf')) {
            throw new \Contao\CoreBundle\Exception\AccessDeniedException('Not enough permissions to publish/unpublish company item ID '.$intId.'.');
        }

        // Set the current record
        if ($dc) {
            $objRow = \Database::getInstance()->prepare('SELECT * FROM '.NewsTwitterAccountsModel::getTable().' WHERE id=?')
                ->limit(1)
                ->execute($intId);

            if ($objRow->numRows) {
                $dc->activeRecord = $objRow;
            }
        }

        $objVersions = new \Versions(NewsTwitterAccountsModel::getTable(), $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA'][NewsTwitterAccountsModel::getTable()]['fields']['published']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA'][NewsTwitterAccountsModel::getTable()]['fields']['published']['save_callback'] as $callback) {
                if (\is_array($callback)) {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, $dc);
                } elseif (\is_callable($callback)) {
                    $blnVisible = $callback($blnVisible, $dc);
                }
            }
        }

        $time = time();

        // Update the database
        \Database::getInstance()->prepare('UPDATE '.NewsTwitterAccountsModel::getTable()." SET tstamp=$time, published='".($blnVisible ? '1' : '')."' WHERE id=?")
            ->execute($intId);

        if ($dc) {
            $dc->activeRecord->tstamp = $time;
            $dc->activeRecord->published = ($blnVisible ? '1' : '');
        }

        // Trigger the onsubmit_callback
        if (\is_array($GLOBALS['TL_DCA'][NewsTwitterAccountsModel::getTable()]['config']['onsubmit_callback'])) {
            foreach ($GLOBALS['TL_DCA'][NewsTwitterAccountsModel::getTable()]['config']['onsubmit_callback'] as $callback) {
                if (\is_array($callback)) {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                } elseif (\is_callable($callback)) {
                    $callback($dc);
                }
            }
        }

        $objVersions->create();
    }
}
