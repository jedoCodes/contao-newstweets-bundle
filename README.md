# Contao 4 NewsTweets Bundle

[![Latest Stable Version](https://poser.pugx.org/jedolabs/contao-newstweets-bundle/v/stable.svg)](https://packagist.org/packages/jedolabs/contao-newstweets-bundle) 
[![Total Downloads](https://poser.pugx.org/jedolabs/contao-newstweets-bundle/downloads.svg)](https://packagist.org/packages/jedolabs/contao-newstweets-bundle) 
[![Latest Unstable Version](https://poser.pugx.org/jedolabs/contao-newstweets-bundle/v/unstable.svg)](https://packagist.org/packages/jedolabs/contao-newstweets-bundle) 
[![License](https://poser.pugx.org/jedolabs/contao-newstweets-bundle/license.svg)](https://packagist.org/packages/jedolabs/contao-newstweets-bundle)

## About

This bundle adds Contao News tweets functionality. 
This makes managing News posts on Twitter a breeze. In the backend of Contao, your editors can easily manage the news posts.


### Installation

* Installation Guide (EN): [INSTALLATION_EN.md](INSTALLATION_EN.md)
* Installationsanleitung (DE): [INSTALLATION_DE.md](INSTALLATION_DE.md)


### User guide (Coming soon!)

* English: https://docs.jedo-labs.de/en/ContaoNewsTweetsBundle.html
* German: https://docs.jedo-labs.de/de/ContaoNewsTweetsBundle.html


